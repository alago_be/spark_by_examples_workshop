You'll need jupyter:
`brew install jupyter`

And Apache Toree (Scala/Spark kernel)
https://toree.apache.org/download/

`pip install --upgrade toree`

And, finally, a spark Distribution
What i usually do is to have an `SPARK_HOME=~/spark`
and that is a soft link to a real spark binaries

`ln -sf spark /path/to/real/spark`

That way I can switch versions fast

Samples are based on Spark 2.4.5


cd to this repo

`jupyter-notebook`

Notebook 1:

- Column to Column transformations, without and with params
- Renaming to achieve duplication of columns. Optimized logical plan explain
- Composition of transformations

Notebook 2:

- Working with simple arrays and transformations
- How to work around that we are using Spark 2.3.5 and not 3.x thorugh expr and Strings

Notebook 3 (To be added):

- Working with arrays in any level of nestedness

Suggestions on more examples are accepted


=== Resources:

These are the sql functions we should stick to. Using just sql and no UDFs make all faster 
(Doc on this issue: https://medium.com/@fqaiser94/udfs-vs-map-vs-custom-spark-native-functions-91ab2c154b44)

https://spark.apache.org/docs/2.4.3/api/scala/index.html#org.apache.spark.sql.functions$
https://spark.apache.org/docs/2.4.3/api/sql/index.html

Also, favor higher order functions over explode + collect:
https://databricks.com/blog/2018/11/16/introducing-new-built-in-functions-and-higher-order-functions-for-complex-data-types-in-apache-spark.html



