Things covered in this Notebook:

 - Column to Column transformations, without and with params
 - Renaming to achieve duplication of columns. Optimized logical plan explain
 - Composition of transformations
 


```scala
val dates = Seq("1601510399", "1601318756", "1502817295")
val ds = dates.toDS
ds.show(false)
```

    +----------+
    |value     |
    +----------+
    |1601510399|
    |1601318756|
    |1502817295|
    +----------+
    



    dates = List(1601510399, 1601318756, 1502817295)
    ds = [value: string]






    [value: string]




```scala
import org.apache.spark.sql.types._
import org.apache.spark.sql.functions._
// Convert to timestamp
ds.withColumn("timestamp", date_format(col("value"), "dd.MM.yyyy")).show
```

    +----------+---------+
    |     value|timestamp|
    +----------+---------+
    |1601510399|     null|
    |1601318756|     null|
    |1502817295|     null|
    +----------+---------+
    



```scala
// uh, it doesnt understand strings with longs, and returns null, lets try with a timestamp
var with_timestamp = ds.withColumn("timestamp", from_unixtime(col("value")))
with_timestamp.show

println("Convert column to the specified value") 
with_timestamp = with_timestamp.withColumn("timestamp",  date_format(col("timestamp"), "dd.MM.yyyy"))
with_timestamp.show
```

    +----------+-------------------+
    |     value|          timestamp|
    +----------+-------------------+
    |1601510399|2020-09-30 23:59:59|
    |1601318756|2020-09-28 18:45:56|
    |1502817295|2017-08-15 17:14:55|
    +----------+-------------------+
    
    Convert column to the specified value
    +----------+----------+
    |     value| timestamp|
    +----------+----------+
    |1601510399|30.09.2020|
    |1601318756|28.09.2020|
    |1502817295|15.08.2017|
    +----------+----------+
    



    with_timestamp = [value: string, timestamp: string]
    with_timestamp = [value: string, timestamp: string]






    [value: string, timestamp: string]




```scala
// All in one step:
var with_timestamp = ds.withColumn("timestamp",date_format( from_unixtime(col("value")), "dd.MM.yyyy"))
with_timestamp.show
```

    +----------+----------+
    |     value| timestamp|
    +----------+----------+
    |1601510399|30.09.2020|
    |1601318756|28.09.2020|
    |1502817295|15.08.2017|
    +----------+----------+
    



    with_timestamp = [value: string, timestamp: string]






    [value: string, timestamp: string]




```scala
// Actually, from_unixtime can apply the format, but this was for demos
ds.withColumn("timestamp",from_unixtime(col("value"), "dd.MM.yyyy")).show
```

    +----------+----------+
    |     value| timestamp|
    +----------+----------+
    |1601510399|30.09.2020|
    |1601318756|28.09.2020|
    |1502817295|15.08.2017|
    +----------+----------+
    



```scala
import org.apache.spark.sql.Column
// Composing the transformations
val transformationsMap : Map[String, Column => Column] = Map(
  ( "from_unixtime", v => from_unixtime(v)),
  ( "date_format", v => date_format(v, "dd.MM.yyyy"))    
)

// Imagine this is our x-transofrmations array
val transformations: Seq[String] = Seq("from_unixtime", "date_format")

val composed = transformations.foldLeft(col("value"))((acc, t) => {
      transformationsMap(t)(acc)
    })


ds.withColumn("timestamp",composed).show
```

    +----------+----------+
    |     value| timestamp|
    +----------+----------+
    |1601510399|30.09.2020|
    |1601318756|28.09.2020|
    |1502817295|15.08.2017|
    +----------+----------+
    



    transformationsMap = > org.apache.spark.sql.Column] = Map(from_unixtime -> <function1>, date_format -> <function1>)
    transformations = List(from_unixtime, date_format)
    composed = date_format(from_unixtime(value, yyyy-MM-dd HH:mm:ss), dd.MM.yyyy)






    date_format(from_unixtime(value, yyyy-MM-dd HH:mm:ss), dd.MM.yyyy)




```scala
// Now with params

val transformationsMap : Map[String, (Column, Option[List[String]]) => Column] = Map(
  ( "from_unixtime", (v, p) => from_unixtime(v)),
  ( "date_format", (v, p) => date_format(v, p.getOrElse(List("dd.MM.yyyy")).head ))    
)

// Imagine this is our x-transofrmations array
val transformations: Seq[(String, Option[List[String]])] = Seq( 
    ( "from_unixtime", None),
    ( "date_format", Some(List("dd-MM-yyyy")) )
    )

val composed = transformations.foldLeft(col("value"))((acc, t) => {
      transformationsMap(t._1)(acc, t._2)
    })


ds.withColumn("timestamp",composed).show
```

    +----------+----------+
    |     value| timestamp|
    +----------+----------+
    |1601510399|30-09-2020|
    |1601318756|28-09-2020|
    |1502817295|15-08-2017|
    +----------+----------+
    



    transformationsMap = > org.apache.spark.sql.Column] = Map(from_unixtime -> <function2>, date_format -> <function2>)
    transformations = List((from_unixtime,None), (date_format,Some(List(dd-MM-yyyy))))
    composed = date_format(from_unixtime(value, yyyy-MM-dd HH:mm:ss), dd-MM-yyyy)






    date_format(from_unixtime(value, yyyy-MM-dd HH:mm:ss), dd-MM-yyyy)




```scala
// What if we want to transform one column, and also create a new one from it. i.e: 2 transformations from same column

val transformations_1: Seq[(String, Option[List[String]])] = Seq( 
    ( "from_unixtime", None),
    ( "date_format", Some(List("dd-MM-yyyy")) )
    )

val composed_1 = transformations_1.foldLeft(col("value"))((acc, t) => {
      transformationsMap(t._1)(acc, t._2)
    })


val transformations_2: Seq[(String, Option[List[String]])] = Seq( 
    ( "from_unixtime", None)
    )

val composed_2 = transformations_2.foldLeft(col("value"))((acc, t) => {
      transformationsMap(t._1)(acc, t._2)
    })

// composed_2 is actually replacing the original column
ds.withColumn("value",composed_2).withColumn("timestamp",composed_1).show

println("No surprise, it fails!")



```

    +-------------------+---------+
    |              value|timestamp|
    +-------------------+---------+
    |2020-09-30 23:59:59|     null|
    |2020-09-28 18:45:56|     null|
    |2017-08-15 17:14:55|     null|
    +-------------------+---------+
    
    No surprise, it fails!



    transformations_1 = List((from_unixtime,None), (date_format,Some(List(dd-MM-yyyy))))
    composed_1 = date_format(from_unixtime(value, yyyy-MM-dd HH:mm:ss), dd-MM-yyyy)
    transformations_2 = List((from_unixtime,None))
    composed_2 = from_unixtime(value, yyyy-MM-dd HH:mm:ss)






    from_unixtime(value, yyyy-MM-dd HH:mm:ss)




```scala
// Lets fix it by generating a temporary original column
// First, lets refactor a bit our code, so we can reuse it:

def createQuery( c: Column, tx: Seq[(String, Option[List[String]])] ): Column = {
    tx.foldLeft(c)((acc, t) => {
      transformationsMap(t._1)(acc, t._2)
    })
}

// Just want to pass a column name, lets just overload the method
def createQuery( c: String, tx: Seq[(String, Option[List[String]])] ): Column = createQuery(col(c), tx)

val column_1_query = createQuery("original_value", transformations_1)
val column_2_query = createQuery("original_value", transformations_2)

// Try again
val two_columns = ds.withColumnRenamed("value","original_value")
.withColumn("value",column_1_query).withColumn("timestamp",column_2_query).
drop("original_value")

two_columns.show

// Check how it optimizes so no renaming is needed at all
two_columns.explain(true)

```

    +----------+-------------------+
    |     value|          timestamp|
    +----------+-------------------+
    |30-09-2020|2020-09-30 23:59:59|
    |28-09-2020|2020-09-28 18:45:56|
    |15-08-2017|2017-08-15 17:14:55|
    +----------+-------------------+
    
    == Parsed Logical Plan ==
    Project [value#91, timestamp#94]
    +- Project [original_value#89, value#91, from_unixtime(cast(original_value#89 as bigint), yyyy-MM-dd HH:mm:ss, Some(UTC)) AS timestamp#94]
       +- Project [original_value#89, date_format(cast(from_unixtime(cast(original_value#89 as bigint), yyyy-MM-dd HH:mm:ss, Some(UTC)) as timestamp), dd-MM-yyyy, Some(UTC)) AS value#91]
          +- Project [value#1 AS original_value#89]
             +- LocalRelation [value#1]
    
    == Analyzed Logical Plan ==
    value: string, timestamp: string
    Project [value#91, timestamp#94]
    +- Project [original_value#89, value#91, from_unixtime(cast(original_value#89 as bigint), yyyy-MM-dd HH:mm:ss, Some(UTC)) AS timestamp#94]
       +- Project [original_value#89, date_format(cast(from_unixtime(cast(original_value#89 as bigint), yyyy-MM-dd HH:mm:ss, Some(UTC)) as timestamp), dd-MM-yyyy, Some(UTC)) AS value#91]
          +- Project [value#1 AS original_value#89]
             +- LocalRelation [value#1]
    
    == Optimized Logical Plan ==
    LocalRelation [value#91, timestamp#94]
    
    == Physical Plan ==
    LocalTableScan [value#91, timestamp#94]



    column_1_query = date_format(from_unixtime(original_value, yyyy-MM-dd HH:mm:ss), dd-MM-yyyy)
    column_2_query = from_unixtime(original_value, yyyy-MM-dd HH:mm:ss)
    two_columns = [value: string, timestamp: string]




    createQuery: (c: org.apache.spark.sql.Column, tx: Seq[(String, Option[List[String]])])org.apache.spark.sql.Column <and> (c: String, tx: Seq[(String, Option[List[String]])])org.apache.spark.sql.Column
    createQuery: (c: org.apache.spark.sql.Column, tx: Seq[(String, Option[List[String]])])org.apache.spark.sql.Column <and> (c: String, tx: Seq[(String, Option[List[String]])])org.apache.spark.sql.Column






    [value: string, timestamp: string]




```scala
// So, what if we have an SQL function that is not yet available through Spark Scala API?
// Wrap it!
// EG: Split
// https://spark.apache.org/docs/2.4.5/api/scala/index.html#org.apache.spark.sql.functions$@split(str:org.apache.spark.sql.Column,pattern:String):org.apache.spark.sql.Column

// Need to redefine some stuff :
val transformationsMap : Map[String, (Column, Option[List[String]]) => Column] = Map(
    ( "from_unixtime", (v, p) => from_unixtime(v)),
    ( "date_format", (v, p) => date_format(v, p.getOrElse(List("dd.MM.yyyy")).head )),
    ( "split",  (v, p) => expr( s""" split($v, '${p.getOrElse(List("[ABC]")).head}' ) """))
)


def createQuery( c: Column, tx: Seq[(String, Option[List[String]])] ): Column = {
    tx.foldLeft(c)((acc, t) => {
      transformationsMap(t._1)(acc, t._2)
    })
}
// Just want to pass a column name, lets just overload the method
def createQuery( c: String, tx: Seq[(String, Option[List[String]])] ): Column = createQuery(col(c), tx)

val transformations: Seq[(String, Option[List[String]])] = Seq( 
    ( "split", None)
    )


val ds_split = Seq("abAbcCghyD", "oneAtwoBthreeC").toDS

val query = createQuery("value", transformations)

// Try again
ds_split.withColumn("value", query).show(false)
```

    +-------------------+
    |value              |
    +-------------------+
    |[ab, bc, ghyD]     |
    |[one, two, three, ]|
    +-------------------+
    



    transformationsMap = > org.apache.spark.sql.Column] = Map(from_unixtime -> <function2>, date_format -> <function2>, split -> <function2>)
    transformations = List((split,None))
    ds_split = [value: string]
    query = split(value, [ABC])




    createQuery: (c: org.apache.spark.sql.Column, tx: Seq[(String, Option[List[String]])])org.apache.spark.sql.Column <and> (c: String, tx: Seq[(String, Option[List[String]])])org.apache.spark.sql.Column
    createQuery: (c: org.apache.spark.sql.Column, tx: Seq[(String, Option[List[String]])])org.apache.spark.sql.Column <and> (c: String, tx: Seq[(String, Option[List[String]])])org.apache.spark.sql.Column






    split(value, [ABC])




```scala
// What if we dont want the more complicated methods directly in the map?


def splitColumn(v: Column, p: Option[List[String]]): Column = {
    val params = p.getOrElse(List("[ABC]")).head
    expr( s"""split($v, '$params' )""")
}

val transformationsMap : Map[String, (Column, Option[List[String]]) => Column] = Map(
    ( "from_unixtime", (v, p) => from_unixtime(v)),
    ( "date_format", (v, p) => date_format(v, p.getOrElse(List("dd.MM.yyyy")).head )),
    ( "split",  splitColumn )
)


def createQuery( c: Column, tx: Seq[(String, Option[List[String]])] ): Column = {
    tx.foldLeft(c)((acc, t) => {
      transformationsMap(t._1)(acc, t._2)
    })
}
// Just want to pass a column name, lets just overload the method
def createQuery( c: String, tx: Seq[(String, Option[List[String]])] ): Column = createQuery(col(c), tx)

val transformations: Seq[(String, Option[List[String]])] = Seq( 
    ( "split", None)
    )


val ds_split = Seq("abAbcCghyD", "oneAtwoBthreeC").toDS

val query = createQuery("value", transformations)

// Try again
ds_split.withColumn("value", query).show(false)

```

    +-------------------+
    |value              |
    +-------------------+
    |[ab, bc, ghyD]     |
    |[one, two, three, ]|
    +-------------------+
    



    transformationsMap = > org.apache.spark.sql.Column] = Map(from_unixtime -> <function2>, date_format -> <function2>, split -> <function2>)
    transformations = List((split,None))




    splitColumn: (v: org.apache.spark.sql.Column, p: Option[List[String]])org.apache.spark.sql.Column
    createQuery: (c: org.apache.spark.sql.Column, tx: Seq[(String, Option[List[String]])])org.apache.spark.sql.Column <and> (c: String, tx: Seq[(String, Option[List[String]])])org.apache.spark.sql.Column
    createQuery: (c: org.apache.spark.sql.Column, tx: Seq[(String, Option[List[String]])])org.apache.spark.sql.Column <and> (c: String, tx: Seq[(String, Option[List[String]])])org.apache.spark.sql.Column
    ds_split: org.apache.spa...






    List((split,None))




```scala
// Let's wrap everything in classes
import org.apache.spark.sql.types._
import org.apache.spark.sql.functions._
import org.apache.spark.sql.{ Column, Dataset }

// This would be the class parsed from the json with the schema
case class Transform( inputCol: String, outputCol: String, transformations: Seq[(String, Option[List[String]])]  )

object Transformer {
   private val transformationsMap : Map[String, (Column, Option[List[String]]) => Column] = Map(
      ( "from_unixtime", (v, p) => from_unixtime(v)),
    ( "date_format", (v, p) => date_format(v, p.getOrElse(List("dd.MM.yyyy")).head )),
    ( "split",  (v, p) => expr( s""" split($v, '${p.getOrElse(List("[ABC]")).head}' ) """))
    )
    

    def createQuery( c: Column, tx: Seq[(String, Option[List[String]])] ): Column = {
        tx.foldLeft(c)((acc, t) => {
            transformationsMap(t._1)(acc, t._2)
      })
    }
    // Just want to pass a column name, lets just overload the method
    def createQuery( c: String, tx: Seq[(String, Option[List[String]])] ): Column = createQuery(col(c), tx)
}

trait BaseTransformer {
    def transform(ds: Dataset[_]) : Dataset[_]
}

class Transfomer(t: Transform) extends BaseTransformer{
    
    val query = Transformer.createQuery( t.inputCol, t.transformations )
    
    def transform(ds: Dataset[_]) : Dataset[_] = {
         ds.withColumn(t.outputCol, query)
    }
}

// Assumes all Transforms have been grouped for the same inputCol
class CompositeTransformer(inputCol: String, t: Seq[Transform]) extends BaseTransformer {
    import java.util.UUID
    
    val renamed_col = UUID.randomUUID.toString + inputCol
    val queries = t.map( t => ( t.outputCol, Transformer.createQuery(renamed_col, t.transformations) ) )
    
    def transform(ds: Dataset[_]) : Dataset[_] = {
        var ds_renamed = ds.withColumnRenamed(inputCol, renamed_col);
        queries.foldLeft(ds_renamed)( ( acc, t ) => {
            acc.withColumn(t._1, t._2)
        })
        ds_renamed.drop(renamed_col)
    }
}

// TODO: input and outputCol can be different


```


    defined class Transform
    defined object Transformer
    defined trait BaseTransformer
    defined class Transfomer
    defined class CompositeTransformer




```scala

// TODO: Add example 


```




    Name: Syntax Error.
    Message: 
    StackTrace: 


