Things covered in this Notebook:

 - Working with simple arrays
 - How to work around that we are using Spark 2.3.5 and not 3.x thorugh expr and Strings


```scala
// Initial setup with things we learned on the previous notebook
import org.apache.spark.sql.Column
import org.apache.spark.sql.types._
import org.apache.spark.sql.functions._

val transformationsMap : Map[String, (Column, Option[List[String]]) => Column] = Map(
    ( "trim",  (v,p) => trim(v) ), // Good ol' trim, the best for demos
    ( "upper",  (v,p) => upper(v) ),
    ( "lower",  (v,p) => lower(v) ),
    ( "from_unixtime", (v, p) => from_unixtime(v)),
    ( "date_format", (v, p) => date_format(v, p.getOrElse(List("dd.MM.yyyy")).head ))
)

def createQuery( c: Column, tx: Seq[(String, Option[List[String]])] ): Column = {
    tx.foldLeft(c)((acc, t) => {
      transformationsMap(t._1)(acc, t._2)
    })
}

// Just want to pass a column name, lets just overload the method
def createQuery( c: String, tx: Seq[(String, Option[List[String]])] ): Column = createQuery(col(c), tx)

```


    transformationsMap = > org.apache.spark.sql.Column] = Map(from_unixtime -> <function2>, lower -> <function2>, upper -> <function2>, date_format -> <function2>, trim -> <function2>)




    createQuery: (c: org.apache.spark.sql.Column, tx: Seq[(String, Option[List[String]])])org.apache.spark.sql.Column <and> (c: String, tx: Seq[(String, Option[List[String]])])org.apache.spark.sql.Column
    createQuery: (c: org.apache.spark.sql.Column, tx: Seq[(String, Option[List[String]])])org.apache.spark.sql.Column <and> (c: String, tx: Seq[(String, Option[List[String]])])org.apache.spark.sql.Column






    > org.apache.spark.sql.Column] = Map(from_unixtime -> <function2>, lower -> <function2>, upper -> <function2>, date_format -> <function2>, trim -> <function2>)




```scala
// What happens when we have an array of values?

val ds = Seq( List("a", "b", "c"), List("t"), List() ).toDS
ds.show(true)
```

    +---------+
    |    value|
    +---------+
    |[a, b, c]|
    |      [t]|
    |       []|
    +---------+
    



    ds = [value: array<string>]






    [value: array<string>]




```scala
ds.withColumn("value", trim(col("value")))
```




    Name: org.apache.spark.sql.AnalysisException
    Message: cannot resolve 'trim(`value`)' due to data type mismatch: argument 1 requires string type, however, '`value`' is of array<string> type.;;
    'Project [trim(value#166, None) AS value#172]
    +- LocalRelation [value#166]
    
    StackTrace: 'Project [trim(value#166, None) AS value#172]
    +- LocalRelation [value#166]
      at org.apache.spark.sql.catalyst.analysis.package$AnalysisErrorAt.failAnalysis(package.scala:42)
      at org.apache.spark.sql.catalyst.analysis.CheckAnalysis$$anonfun$checkAnalysis$1$$anonfun$apply$3.applyOrElse(CheckAnalysis.scala:116)
      at org.apache.spark.sql.catalyst.analysis.CheckAnalysis$$anonfun$checkAnalysis$1$$anonfun$apply$3.applyOrElse(CheckAnalysis.scala:108)
      at org.apache.spark.sql.catalyst.trees.TreeNode$$anonfun$transformUp$1.apply(TreeNode.scala:280)
      at org.apache.spark.sql.catalyst.trees.TreeNode$$anonfun$transformUp$1.apply(TreeNode.scala:280)
      at org.apache.spark.sql.catalyst.trees.CurrentOrigin$.withOrigin(TreeNode.scala:69)
      at org.apache.spark.sql.catalyst.trees.TreeNode.transformUp(TreeNode.scala:279)
      at org.apache.spark.sql.catalyst.trees.TreeNode$$anonfun$3.apply(TreeNode.scala:277)
      at org.apache.spark.sql.catalyst.trees.TreeNode$$anonfun$3.apply(TreeNode.scala:277)
      at org.apache.spark.sql.catalyst.trees.TreeNode$$anonfun$4.apply(TreeNode.scala:328)
      at org.apache.spark.sql.catalyst.trees.TreeNode.mapProductIterator(TreeNode.scala:186)
      at org.apache.spark.sql.catalyst.trees.TreeNode.mapChildren(TreeNode.scala:326)
      at org.apache.spark.sql.catalyst.trees.TreeNode.transformUp(TreeNode.scala:277)
      at org.apache.spark.sql.catalyst.plans.QueryPlan$$anonfun$transformExpressionsUp$1.apply(QueryPlan.scala:93)
      at org.apache.spark.sql.catalyst.plans.QueryPlan$$anonfun$transformExpressionsUp$1.apply(QueryPlan.scala:93)
      at org.apache.spark.sql.catalyst.plans.QueryPlan$$anonfun$1.apply(QueryPlan.scala:105)
      at org.apache.spark.sql.catalyst.plans.QueryPlan$$anonfun$1.apply(QueryPlan.scala:105)
      at org.apache.spark.sql.catalyst.trees.CurrentOrigin$.withOrigin(TreeNode.scala:69)
      at org.apache.spark.sql.catalyst.plans.QueryPlan.transformExpression$1(QueryPlan.scala:104)
      at org.apache.spark.sql.catalyst.plans.QueryPlan.org$apache$spark$sql$catalyst$plans$QueryPlan$$recursiveTransform$1(QueryPlan.scala:116)
      at org.apache.spark.sql.catalyst.plans.QueryPlan$$anonfun$org$apache$spark$sql$catalyst$plans$QueryPlan$$recursiveTransform$1$2.apply(QueryPlan.scala:121)
      at scala.collection.TraversableLike$$anonfun$map$1.apply(TraversableLike.scala:234)
      at scala.collection.TraversableLike$$anonfun$map$1.apply(TraversableLike.scala:234)
      at scala.collection.immutable.List.foreach(List.scala:392)
      at scala.collection.TraversableLike$class.map(TraversableLike.scala:234)
      at scala.collection.immutable.List.map(List.scala:296)
      at org.apache.spark.sql.catalyst.plans.QueryPlan.org$apache$spark$sql$catalyst$plans$QueryPlan$$recursiveTransform$1(QueryPlan.scala:121)
      at org.apache.spark.sql.catalyst.plans.QueryPlan$$anonfun$2.apply(QueryPlan.scala:126)
      at org.apache.spark.sql.catalyst.trees.TreeNode.mapProductIterator(TreeNode.scala:186)
      at org.apache.spark.sql.catalyst.plans.QueryPlan.mapExpressions(QueryPlan.scala:126)
      at org.apache.spark.sql.catalyst.plans.QueryPlan.transformExpressionsUp(QueryPlan.scala:93)
      at org.apache.spark.sql.catalyst.analysis.CheckAnalysis$$anonfun$checkAnalysis$1.apply(CheckAnalysis.scala:108)
      at org.apache.spark.sql.catalyst.analysis.CheckAnalysis$$anonfun$checkAnalysis$1.apply(CheckAnalysis.scala:86)
      at org.apache.spark.sql.catalyst.trees.TreeNode.foreachUp(TreeNode.scala:126)
      at org.apache.spark.sql.catalyst.analysis.CheckAnalysis$class.checkAnalysis(CheckAnalysis.scala:86)
      at org.apache.spark.sql.catalyst.analysis.Analyzer.checkAnalysis(Analyzer.scala:95)
      at org.apache.spark.sql.catalyst.analysis.Analyzer$$anonfun$executeAndCheck$1.apply(Analyzer.scala:108)
      at org.apache.spark.sql.catalyst.analysis.Analyzer$$anonfun$executeAndCheck$1.apply(Analyzer.scala:105)
      at org.apache.spark.sql.catalyst.plans.logical.AnalysisHelper$.markInAnalyzer(AnalysisHelper.scala:201)
      at org.apache.spark.sql.catalyst.analysis.Analyzer.executeAndCheck(Analyzer.scala:105)
      at org.apache.spark.sql.execution.QueryExecution.analyzed$lzycompute(QueryExecution.scala:58)
      at org.apache.spark.sql.execution.QueryExecution.analyzed(QueryExecution.scala:56)
      at org.apache.spark.sql.execution.QueryExecution.assertAnalyzed(QueryExecution.scala:48)
      at org.apache.spark.sql.Dataset$.ofRows(Dataset.scala:78)
      at org.apache.spark.sql.Dataset.org$apache$spark$sql$Dataset$$withPlan(Dataset.scala:3412)
      at org.apache.spark.sql.Dataset.select(Dataset.scala:1340)
      at org.apache.spark.sql.Dataset.withColumns(Dataset.scala:2258)
      at org.apache.spark.sql.Dataset.withColumn(Dataset.scala:2225)


Name: org.apache.spark.sql.AnalysisException
Message: cannot resolve 'trim(`value`)' due to data type mismatch: argument 1 requires string type, however, '`value`' is of array<string> type.;;
'Project [trim(value#11, None) AS value#17]
+- LocalRelation [value#11]

```scala
// Lets use transform

ds.withColumn("value", transform(col("value"), x => trim(x)) )
// Ops, it isnt availble til spark 3.0

```




    Name: Unknown Error
    Message: lastException: Throwable = null
    <console>:51: error: not found: value transform
           ds.withColumn("value", transform(col("value"), x => trim(x)) )
                                  ^
    
    StackTrace: 




```scala
// but it is a spark-sql function already!

val myCol = "value"
val tx = s""" transform(${myCol}, x -> upper(x)) """
println(tx)

ds.withColumn("value", expr(tx) ).show(true)
```

     transform(value, x -> upper(x)) 
    +---------+
    |    value|
    +---------+
    |[A, B, C]|
    |      [T]|
    |       []|
    +---------+
    



    myCol = value
    tx = " transform(value, x -> upper(x)) "






    " transform(value, x -> upper(x)) "




```scala
// Now it works, but how can we wrap it with the createQuery we defined before?
// This is the transform we want to apply:
val transformations: Seq[(String, Option[List[String]])] = Seq( 
    ( "from_unixtime", None)
    )

val query_1 = createQuery("value", transformations)

println( "The class of a query is a column. just what transform takes!" )
println( query_1.getClass )

```

    The class of a query is a column. just what transform takes!
    class org.apache.spark.sql.Column



    transformations = List((from_unixtime,None))
    query_1 = from_unixtime(value, yyyy-MM-dd HH:mm:ss)






    from_unixtime(value, yyyy-MM-dd HH:mm:ss)




```scala
// but, this wont work. Spark SQL doesnt know about query_1

val myCol = "value"
val tx = s""" transform(${myCol}, x -> query_1(x)) """
println(tx)
ds.withColumn("value", expr(tx) ).show(true)
```

     transform(value, x -> query_1(x)) 





    Name: org.apache.spark.sql.AnalysisException
    Message: Undefined function: 'query_1'. This function is neither a registered temporary function nor a permanent function registered in the database 'default'.; line 1 pos 23
    StackTrace:   at org.apache.spark.sql.catalyst.analysis.Analyzer$LookupFunctions$$anonfun$apply$15$$anonfun$applyOrElse$49.apply(Analyzer.scala:1291)
      at org.apache.spark.sql.catalyst.analysis.Analyzer$LookupFunctions$$anonfun$apply$15$$anonfun$applyOrElse$49.apply(Analyzer.scala:1291)
      at org.apache.spark.sql.catalyst.analysis.package$.withPosition(package.scala:53)
      at org.apache.spark.sql.catalyst.analysis.Analyzer$LookupFunctions$$anonfun$apply$15.applyOrElse(Analyzer.scala:1290)
      at org.apache.spark.sql.catalyst.analysis.Analyzer$LookupFunctions$$anonfun$apply$15.applyOrElse(Analyzer.scala:1282)
      at org.apache.spark.sql.catalyst.trees.TreeNode$$anonfun$2.apply(TreeNode.scala:258)
      at org.apache.spark.sql.catalyst.trees.TreeNode$$anonfun$2.apply(TreeNode.scala:258)
      at org.apache.spark.sql.catalyst.trees.CurrentOrigin$.withOrigin(TreeNode.scala:69)
      at org.apache.spark.sql.catalyst.trees.TreeNode.transformDown(TreeNode.scala:257)
      at org.apache.spark.sql.catalyst.trees.TreeNode$$anonfun$transformDown$1.apply(TreeNode.scala:263)
      at org.apache.spark.sql.catalyst.trees.TreeNode$$anonfun$transformDown$1.apply(TreeNode.scala:263)
      at org.apache.spark.sql.catalyst.trees.TreeNode$$anonfun$4.apply(TreeNode.scala:328)
      at org.apache.spark.sql.catalyst.trees.TreeNode.mapProductIterator(TreeNode.scala:186)
      at org.apache.spark.sql.catalyst.trees.TreeNode.mapChildren(TreeNode.scala:326)
      at org.apache.spark.sql.catalyst.trees.TreeNode.transformDown(TreeNode.scala:263)
      at org.apache.spark.sql.catalyst.trees.TreeNode$$anonfun$transformDown$1.apply(TreeNode.scala:263)
      at org.apache.spark.sql.catalyst.trees.TreeNode$$anonfun$transformDown$1.apply(TreeNode.scala:263)
      at org.apache.spark.sql.catalyst.trees.TreeNode.org$apache$spark$sql$catalyst$trees$TreeNode$$mapChild$2(TreeNode.scala:297)
      at org.apache.spark.sql.catalyst.trees.TreeNode$$anonfun$4$$anonfun$apply$13.apply(TreeNode.scala:356)
      at scala.collection.TraversableLike$$anonfun$map$1.apply(TraversableLike.scala:234)
      at scala.collection.TraversableLike$$anonfun$map$1.apply(TraversableLike.scala:234)
      at scala.collection.mutable.ResizableArray$class.foreach(ResizableArray.scala:59)
      at scala.collection.mutable.ArrayBuffer.foreach(ArrayBuffer.scala:48)
      at scala.collection.TraversableLike$class.map(TraversableLike.scala:234)
      at scala.collection.AbstractTraversable.map(Traversable.scala:104)
      at org.apache.spark.sql.catalyst.trees.TreeNode$$anonfun$4.apply(TreeNode.scala:356)
      at org.apache.spark.sql.catalyst.trees.TreeNode.mapProductIterator(TreeNode.scala:186)
      at org.apache.spark.sql.catalyst.trees.TreeNode.mapChildren(TreeNode.scala:326)
      at org.apache.spark.sql.catalyst.trees.TreeNode.transformDown(TreeNode.scala:263)
      at org.apache.spark.sql.catalyst.trees.TreeNode$$anonfun$transformDown$1.apply(TreeNode.scala:263)
      at org.apache.spark.sql.catalyst.trees.TreeNode$$anonfun$transformDown$1.apply(TreeNode.scala:263)
      at org.apache.spark.sql.catalyst.trees.TreeNode$$anonfun$4.apply(TreeNode.scala:328)
      at org.apache.spark.sql.catalyst.trees.TreeNode.mapProductIterator(TreeNode.scala:186)
      at org.apache.spark.sql.catalyst.trees.TreeNode.mapChildren(TreeNode.scala:326)
      at org.apache.spark.sql.catalyst.trees.TreeNode.transformDown(TreeNode.scala:263)
      at org.apache.spark.sql.catalyst.plans.QueryPlan$$anonfun$transformExpressionsDown$1.apply(QueryPlan.scala:83)
      at org.apache.spark.sql.catalyst.plans.QueryPlan$$anonfun$transformExpressionsDown$1.apply(QueryPlan.scala:83)
      at org.apache.spark.sql.catalyst.plans.QueryPlan$$anonfun$1.apply(QueryPlan.scala:105)
      at org.apache.spark.sql.catalyst.plans.QueryPlan$$anonfun$1.apply(QueryPlan.scala:105)
      at org.apache.spark.sql.catalyst.trees.CurrentOrigin$.withOrigin(TreeNode.scala:69)
      at org.apache.spark.sql.catalyst.plans.QueryPlan.transformExpression$1(QueryPlan.scala:104)
      at org.apache.spark.sql.catalyst.plans.QueryPlan.org$apache$spark$sql$catalyst$plans$QueryPlan$$recursiveTransform$1(QueryPlan.scala:116)
      at org.apache.spark.sql.catalyst.plans.QueryPlan$$anonfun$org$apache$spark$sql$catalyst$plans$QueryPlan$$recursiveTransform$1$2.apply(QueryPlan.scala:121)
      at scala.collection.TraversableLike$$anonfun$map$1.apply(TraversableLike.scala:234)
      at scala.collection.TraversableLike$$anonfun$map$1.apply(TraversableLike.scala:234)
      at scala.collection.immutable.List.foreach(List.scala:392)
      at scala.collection.TraversableLike$class.map(TraversableLike.scala:234)
      at scala.collection.immutable.List.map(List.scala:296)
      at org.apache.spark.sql.catalyst.plans.QueryPlan.org$apache$spark$sql$catalyst$plans$QueryPlan$$recursiveTransform$1(QueryPlan.scala:121)
      at org.apache.spark.sql.catalyst.plans.QueryPlan$$anonfun$2.apply(QueryPlan.scala:126)
      at org.apache.spark.sql.catalyst.trees.TreeNode.mapProductIterator(TreeNode.scala:186)
      at org.apache.spark.sql.catalyst.plans.QueryPlan.mapExpressions(QueryPlan.scala:126)
      at org.apache.spark.sql.catalyst.plans.QueryPlan.transformExpressionsDown(QueryPlan.scala:83)
      at org.apache.spark.sql.catalyst.plans.QueryPlan.transformExpressions(QueryPlan.scala:74)
      at org.apache.spark.sql.catalyst.plans.logical.AnalysisHelper$$anonfun$resolveExpressions$1.applyOrElse(AnalysisHelper.scala:129)
      at org.apache.spark.sql.catalyst.plans.logical.AnalysisHelper$$anonfun$resolveExpressions$1.applyOrElse(AnalysisHelper.scala:128)
      at org.apache.spark.sql.catalyst.plans.logical.AnalysisHelper$$anonfun$resolveOperatorsDown$1$$anonfun$2.apply(AnalysisHelper.scala:108)
      at org.apache.spark.sql.catalyst.plans.logical.AnalysisHelper$$anonfun$resolveOperatorsDown$1$$anonfun$2.apply(AnalysisHelper.scala:108)
      at org.apache.spark.sql.catalyst.trees.CurrentOrigin$.withOrigin(TreeNode.scala:69)
      at org.apache.spark.sql.catalyst.plans.logical.AnalysisHelper$$anonfun$resolveOperatorsDown$1.apply(AnalysisHelper.scala:107)
      at org.apache.spark.sql.catalyst.plans.logical.AnalysisHelper$$anonfun$resolveOperatorsDown$1.apply(AnalysisHelper.scala:106)
      at org.apache.spark.sql.catalyst.plans.logical.AnalysisHelper$.allowInvokingTransformsInAnalyzer(AnalysisHelper.scala:194)
      at org.apache.spark.sql.catalyst.plans.logical.AnalysisHelper$class.resolveOperatorsDown(AnalysisHelper.scala:106)
      at org.apache.spark.sql.catalyst.plans.logical.LogicalPlan.resolveOperatorsDown(LogicalPlan.scala:29)
      at org.apache.spark.sql.catalyst.plans.logical.AnalysisHelper$class.resolveOperators(AnalysisHelper.scala:73)
      at org.apache.spark.sql.catalyst.plans.logical.LogicalPlan.resolveOperators(LogicalPlan.scala:29)
      at org.apache.spark.sql.catalyst.plans.logical.AnalysisHelper$class.resolveExpressions(AnalysisHelper.scala:128)
      at org.apache.spark.sql.catalyst.plans.logical.LogicalPlan.resolveExpressions(LogicalPlan.scala:29)
      at org.apache.spark.sql.catalyst.analysis.Analyzer$LookupFunctions$.apply(Analyzer.scala:1282)
      at org.apache.spark.sql.catalyst.analysis.Analyzer$LookupFunctions$.apply(Analyzer.scala:1279)
      at org.apache.spark.sql.catalyst.rules.RuleExecutor$$anonfun$execute$1$$anonfun$apply$1.apply(RuleExecutor.scala:87)
      at org.apache.spark.sql.catalyst.rules.RuleExecutor$$anonfun$execute$1$$anonfun$apply$1.apply(RuleExecutor.scala:84)
      at scala.collection.IndexedSeqOptimized$class.foldl(IndexedSeqOptimized.scala:57)
      at scala.collection.IndexedSeqOptimized$class.foldLeft(IndexedSeqOptimized.scala:66)
      at scala.collection.mutable.WrappedArray.foldLeft(WrappedArray.scala:35)
      at org.apache.spark.sql.catalyst.rules.RuleExecutor$$anonfun$execute$1.apply(RuleExecutor.scala:84)
      at org.apache.spark.sql.catalyst.rules.RuleExecutor$$anonfun$execute$1.apply(RuleExecutor.scala:76)
      at scala.collection.immutable.List.foreach(List.scala:392)
      at org.apache.spark.sql.catalyst.rules.RuleExecutor.execute(RuleExecutor.scala:76)
      at org.apache.spark.sql.catalyst.analysis.Analyzer.org$apache$spark$sql$catalyst$analysis$Analyzer$$executeSameContext(Analyzer.scala:127)
      at org.apache.spark.sql.catalyst.analysis.Analyzer.execute(Analyzer.scala:121)
      at org.apache.spark.sql.catalyst.analysis.Analyzer$$anonfun$executeAndCheck$1.apply(Analyzer.scala:106)
      at org.apache.spark.sql.catalyst.analysis.Analyzer$$anonfun$executeAndCheck$1.apply(Analyzer.scala:105)
      at org.apache.spark.sql.catalyst.plans.logical.AnalysisHelper$.markInAnalyzer(AnalysisHelper.scala:201)
      at org.apache.spark.sql.catalyst.analysis.Analyzer.executeAndCheck(Analyzer.scala:105)
      at org.apache.spark.sql.execution.QueryExecution.analyzed$lzycompute(QueryExecution.scala:58)
      at org.apache.spark.sql.execution.QueryExecution.analyzed(QueryExecution.scala:56)
      at org.apache.spark.sql.execution.QueryExecution.assertAnalyzed(QueryExecution.scala:48)
      at org.apache.spark.sql.Dataset$.ofRows(Dataset.scala:78)
      at org.apache.spark.sql.Dataset.org$apache$spark$sql$Dataset$$withPlan(Dataset.scala:3412)
      at org.apache.spark.sql.Dataset.select(Dataset.scala:1340)
      at org.apache.spark.sql.Dataset.withColumns(Dataset.scala:2258)
      at org.apache.spark.sql.Dataset.withColumn(Dataset.scala:2225)




```scala
// Lets just make another workaround for 2.4.5 

val transformations: Seq[(String, Option[List[String]])] = Seq( 
    ( "upper", None)
    )
//
val query_1 = createQuery("value", transformations)
println("This is the query translated to sql " )
println( query_1.expr.sql )
```

    This is the query translated to sql 
    upper(`value`)



    transformations = List((upper,None))
    query_1 = upper(value)




    lastException: Throwable = null






    upper(value)




```scala

// Lets wrap it in a transform:

val query_sql =  query_1.expr.sql
val myCol = "value" 
println(s"""transform($myCol, x -> $query_sql)""")

// hmmm, something is failing
var query_sql_2 = query_sql.replace("value", "x")

val arrayQuery = s"""transform($myCol, x -> $query_sql_2)"""
println(arrayQuery)

ds.withColumn("value", expr(arrayQuery) ).show(true)
```

    transform(value, x -> upper(`value`))
    transform(value, x -> upper(`x`))
    +---------+
    |    value|
    +---------+
    |[A, B, C]|
    |      [T]|
    |       []|
    +---------+
    



    query_sql = upper(`value`)
    myCol = value
    query_sql_2 = upper(`x`)
    arrayQuery = transform(value, x -> upper(`x`))






    transform(value, x -> upper(`x`))




```scala
// Lets wrap it in a method

def createArrayQuery(c: String, tx: Seq[(String, Option[List[String]])]): Column = {
  
    val q = createQuery(c, tx).expr.sql.replace( c, "x" )

    val arrayQuery = s"transform($c, x -> $q)"

    expr(arrayQuery)
  }

val newArrayQuery = createArrayQuery("value", transformations)

ds.withColumn("value", newArrayQuery ).show(true)
```

    +---------+
    |    value|
    +---------+
    |[A, B, C]|
    |      [T]|
    |       []|
    +---------+
    



    newArrayQuery = transform(value, lambdafunction(upper(x), x))




    createArrayQuery: (c: String, tx: Seq[(String, Option[List[String]])])org.apache.spark.sql.Column






    transform(value, lambdafunction(upper(x), x))




```scala
// One more example

// Failing for one of the values inside an array
val dates = Seq(Seq("1601510399", "   1601318756", "1502817295"), Seq("0901325982"), Seq(), Seq(null) )
val dates_ds = dates.toDS
dates_ds.show(false)
```

    +---------------------------------------+
    |value                                  |
    +---------------------------------------+
    |[1601510399,    1601318756, 1502817295]|
    |[0901325982]                           |
    |[]                                     |
    |[]                                     |
    +---------------------------------------+
    



    dates = List(List(1601510399, "   1601318756", 1502817295), List(0901325982), List(), List(null))
    dates_ds = [value: array<string>]






    [value: array<string>]




```scala
var transformations: Seq[(String, Option[List[String]])] = Seq( 
    ( "from_unixtime", None),
    ( "date_format", Some(List("dd-MM-yyyy")) )
    )


var newArrayQuery = createArrayQuery("value", transformations)

dates_ds.withColumn("value", newArrayQuery ).show(false)
```

    +-------------------------+
    |value                    |
    +-------------------------+
    |[30-09-2020,, 15-08-2017]|
    |[25-07-1998]             |
    |[]                       |
    |[]                       |
    +-------------------------+
    



    transformations = List((from_unixtime,None), (date_format,Some(List(dd-MM-yyyy))))
    newArrayQuery = transform(value, lambdafunction(date_format(from_unixtime(x, yyyy-MM-dd HH:mm:ss), dd-MM-yyyy), x))






    transform(value, lambdafunction(date_format(from_unixtime(x, yyyy-MM-dd HH:mm:ss), dd-MM-yyyy), x))




```scala

transformations = Seq( 
    ( "trim", None),
    ( "from_unixtime", None),
    ( "date_format", Some(List("dd-MM-yyyy")) )
    )


newArrayQuery = createArrayQuery("value", transformations)

dates_ds.withColumn("value", newArrayQuery ).show(false)
```

    +------------------------------------+
    |value                               |
    +------------------------------------+
    |[30-09-2020, 28-09-2020, 15-08-2017]|
    |[25-07-1998]                        |
    |[]                                  |
    |[]                                  |
    +------------------------------------+
    



    transformations = List((trim,None), (from_unixtime,None), (date_format,Some(List(dd-MM-yyyy))))
    newArrayQuery = transform(value, lambdafunction(date_format(from_unixtime(trim(x), yyyy-MM-dd HH:mm:ss), dd-MM-yyyy), x))






    transform(value, lambdafunction(date_format(from_unixtime(trim(x), yyyy-MM-dd HH:mm:ss), dd-MM-yyyy), x))


