Things covered in this Notebook:

 - Extract values from arrays
 - Work with any level of nesting in arrays


```scala
// Lets create our test data
val dataAsJsonString = Seq( """{"first_name":"James","last_name":"Butterburg","addresses":[{"street":"6649 N Blue Gum St","city":"New Orleans","state":"LA","zip":"70116"},{"street":"321 Elm St","city":"Miami","state":"FL","zip":"33142"}],"phoneNumbers":["34999444333","31777555900"]}""").toDS

// Think this is deprecated
val ds = spark.read.json(dataAsJsonString)

ds.show(false)

```

    +------------------------------------------------------------------------------+----------+----------+--------------------------+
    |addresses                                                                     |first_name|last_name |phoneNumbers              |
    +------------------------------------------------------------------------------+----------+----------+--------------------------+
    |[[New Orleans, LA, 6649 N Blue Gum St, 70116], [Miami, FL, 321 Elm St, 33142]]|James     |Butterburg|[34999444333, 31777555900]|
    +------------------------------------------------------------------------------+----------+----------+--------------------------+
    



    dataAsJsonString = [value: string]
    ds = [addresses: array<struct<city:string,state:string,street:string,zip:string>>, first_name: string ... 2 more fields]






    [addresses: array<struct<city:string,state:string,street:string,zip:string>>, first_name: string ... 2 more fields]




```scala

import org.apache.spark.sql.functions._
//lets put each number in its columns

// sql index start at 1
ds.withColumn("phoneNumber_1", element_at(col("phoneNumbers"), 1))
  .withColumn("phoneNumber_2", element_at(col("phoneNumbers"), 2))
  .withColumn("phoneNumber_3", element_at(col("phoneNumbers"), 3))
  .drop("phoneNumbers")
  .drop("addresses").show(false)
```

    +----------+----------+-------------+-------------+-------------+
    |first_name|last_name |phoneNumber_1|phoneNumber_2|phoneNumber_3|
    +----------+----------+-------------+-------------+-------------+
    |James     |Butterburg|34999444333  |31777555900  |null         |
    +----------+----------+-------------+-------------+-------------+
    



```scala
// What if we want something from an array of structs
// With one single nesting thats easy
// You can conveniently just use dot notation and it will return an array
ds.select("addresses.zip").show(false)

ds.withColumn("zip_1", element_at(col("addresses.zip"), 1))
.withColumn("zip_2", element_at(col("addresses.zip"), 2))
.withColumn("zip_3", element_at(col("addresses.zip"), 3))
  .drop("phoneNumbers")
  .drop("addresses").show(false)

```

    +--------------+
    |zip           |
    +--------------+
    |[70116, 33142]|
    +--------------+
    
    +----------+----------+-----+-----+-----+
    |first_name|last_name |zip_1|zip_2|zip_3|
    +----------+----------+-----+-----+-----+
    |James     |Butterburg|70116|33142|null |
    +----------+----------+-----+-----+-----+
    



```scala
// But, wait! I want to apply some transforms on it!
// Lets create some helper methods
import org.apache.spark.sql.Column

val transformationsMap : Map[String, (Column, Option[List[String]]) => Column] = Map(
    ( "lower", (v, p) => lower(v)),
    ( "element_at", (v, p) => element_at(v, p.get.head.toInt ) )
)

def createQuery( c: Column, tx: Seq[(String, Option[List[String]])] ): Column = {
        tx.foldLeft(c)((acc, t) => {
            transformationsMap(t._1)(acc, t._2)
      })
}

// Just want to pass a column name, lets just overload the method
def createQuery( c: String, tx: Seq[(String, Option[List[String]])] ): Column = createQuery(col(c), tx)

def createArrayQuery(c: String, tx: Seq[(String, Option[List[String]])]): Column = {
  
    val q = createQuery(c, tx).expr.sql.replace( c, "x" )

    val arrayQuery = s"transform($c, x -> $q)"

    expr(arrayQuery)
  }


```


    transformationsMap = > org.apache.spark.sql.Column] = Map(lower -> <function2>, element_at -> <function2>)




    createQuery: (c: org.apache.spark.sql.Column, tx: Seq[(String, Option[List[String]])])org.apache.spark.sql.Column <and> (c: String, tx: Seq[(String, Option[List[String]])])org.apache.spark.sql.Column
    createQuery: (c: org.apache.spark.sql.Column, tx: Seq[(String, Option[List[String]])])org.apache.spark.sql.Column <and> (c: String, tx: Seq[(String, Option[List[String]])])org.apache.spark.sql.Column
    createArrayQuery: (c: String, tx: Seq[(String, Option[List[String]])])org.apache.spark.sql.Column






    > org.apache.spark.sql.Column] = Map(lower -> <function2>, element_at -> <function2>)




```scala
// I will lower case the state adresses
// First, returning a single array:

var transformations: Seq[(String, Option[List[String]])] = Seq( 
    ( "lower", None)
    )


var newArrayQuery = createArrayQuery("addresses.state", transformations)

val ds_states =  ds.withColumn("states", newArrayQuery )
ds_states.explain(true)
ds_states.show(false)
```

    == Parsed Logical Plan ==
    'Project [addresses#145, first_name#146, last_name#147, phoneNumbers#148, 'transform('addresses.state, lambdafunction('lower(lambda 'x), lambda 'x, false)) AS states#528]
    +- LogicalRDD [addresses#145, first_name#146, last_name#147, phoneNumbers#148], false
    
    == Analyzed Logical Plan ==
    addresses: array<struct<city:string,state:string,street:string,zip:string>>, first_name: string, last_name: string, phoneNumbers: array<string>, states: array<string>
    Project [addresses#145, first_name#146, last_name#147, phoneNumbers#148, transform(addresses#145.state, lambdafunction(lower(lambda x#538), lambda x#538, false)) AS states#528]
    +- LogicalRDD [addresses#145, first_name#146, last_name#147, phoneNumbers#148], false
    
    == Optimized Logical Plan ==
    Project [addresses#145, first_name#146, last_name#147, phoneNumbers#148, transform(addresses#145.state, lambdafunction(lower(lambda x#538), lambda x#538, false)) AS states#528]
    +- LogicalRDD [addresses#145, first_name#146, last_name#147, phoneNumbers#148], false
    
    == Physical Plan ==
    Project [addresses#145, first_name#146, last_name#147, phoneNumbers#148, transform(addresses#145.state, lambdafunction(lower(lambda x#538), lambda x#538, false)) AS states#528]
    +- Scan ExistingRDD[addresses#145,first_name#146,last_name#147,phoneNumbers#148]
    +------------------------------------------------------------------------------+----------+----------+--------------------------+--------+
    |addresses                                                                     |first_name|last_name |phoneNumbers              |states  |
    +------------------------------------------------------------------------------+----------+----------+--------------------------+--------+
    |[[New Orleans, LA, 6649 N Blue Gum St, 70116], [Miami, FL, 321 Elm St, 33142]]|James     |Butterburg|[34999444333, 31777555900]|[la, fl]|
    +------------------------------------------------------------------------------+----------+----------+--------------------------+--------+
    



    transformations = List((lower,None))
    newArrayQuery = transform(addresses.state, lambdafunction(lower(x), x))
    ds_states = [addresses: array<struct<city:string,state:string,street:string,zip:string>>, first_name: string ... 3 more fields]






    [addresses: array<struct<city:string,state:string,street:string,zip:string>>, first_name: string ... 3 more fields]




```scala
// I want them as different columns
var transformations_1: Seq[(String, Option[List[String]])] = Seq( 
    ( "element_at", Some(List("1")) ),
    ( "lower", None)
    )

var transformations_2: Seq[(String, Option[List[String]])] = Seq( 
    ( "element_at", Some(List("2")) ),
    ( "lower", None)
    )

var query_1 = createQuery("addresses.state", transformations_1)
var query_2 = createQuery("addresses.state", transformations_2)

val ds_states =  ds.withColumn("state_1", query_1 ).withColumn("state_2", query_2 )
ds_states.explain(true)
ds_states.show(false)

// See optimized plan, just all in 1 pass
```

    == Parsed Logical Plan ==
    'Project [addresses#145, first_name#146, last_name#147, phoneNumbers#148, state_1#492, lower(element_at('addresses.state, 2)) AS state_2#499]
    +- Project [addresses#145, first_name#146, last_name#147, phoneNumbers#148, lower(element_at(addresses#145.state, 1)) AS state_1#492]
       +- LogicalRDD [addresses#145, first_name#146, last_name#147, phoneNumbers#148], false
    
    == Analyzed Logical Plan ==
    addresses: array<struct<city:string,state:string,street:string,zip:string>>, first_name: string, last_name: string, phoneNumbers: array<string>, state_1: string, state_2: string
    Project [addresses#145, first_name#146, last_name#147, phoneNumbers#148, state_1#492, lower(element_at(addresses#145.state, 2)) AS state_2#499]
    +- Project [addresses#145, first_name#146, last_name#147, phoneNumbers#148, lower(element_at(addresses#145.state, 1)) AS state_1#492]
       +- LogicalRDD [addresses#145, first_name#146, last_name#147, phoneNumbers#148], false
    
    == Optimized Logical Plan ==
    Project [addresses#145, first_name#146, last_name#147, phoneNumbers#148, lower(element_at(addresses#145.state, 1)) AS state_1#492, lower(element_at(addresses#145.state, 2)) AS state_2#499]
    +- LogicalRDD [addresses#145, first_name#146, last_name#147, phoneNumbers#148], false
    
    == Physical Plan ==
    *(1) Project [addresses#145, first_name#146, last_name#147, phoneNumbers#148, lower(element_at(addresses#145.state, 1)) AS state_1#492, lower(element_at(addresses#145.state, 2)) AS state_2#499]
    +- Scan ExistingRDD[addresses#145,first_name#146,last_name#147,phoneNumbers#148]
    +------------------------------------------------------------------------------+----------+----------+--------------------------+-------+-------+
    |addresses                                                                     |first_name|last_name |phoneNumbers              |state_1|state_2|
    +------------------------------------------------------------------------------+----------+----------+--------------------------+-------+-------+
    |[[New Orleans, LA, 6649 N Blue Gum St, 70116], [Miami, FL, 321 Elm St, 33142]]|James     |Butterburg|[34999444333, 31777555900]|la     |fl     |
    +------------------------------------------------------------------------------+----------+----------+--------------------------+-------+-------+
    



    transformations_1 = List((element_at,Some(List(1))), (lower,None))
    transformations_2 = List((element_at,Some(List(2))), (lower,None))
    query_1 = lower(element_at(addresses.state, 1))
    query_2 = lower(element_at(addresses.state, 2))
    ds_states = [addresses: array<struct<city:string,state:string,street:string,zip:string>>, first_name: string ... 4 more fields]






    [addresses: array<struct<city:string,state:string,street:string,zip:string>>, first_name: string ... 4 more fields]




```scala
// TODO:

// Rebuilding a struct after transforming ( No use case for now, thats something only needed in Optima trusted phase, 
// worth to show, anyway) 
// Working with several nesting of arrays. E.g: flow_executions[].offers[].ref
```
